FROM ubuntu:20.04

ADD https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64 /usr/local/bin/gitlab-runner

RUN chmod +x usr/local/bin/gitlab-runner

ENTRYPOINT ["gitlab-runner"]
CMD ["run"]
